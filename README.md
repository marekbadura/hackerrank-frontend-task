![](https://static.jobleads.com/resources/_shared/gfx/logo_JL.svg)

# Jobleads Frontend Recruitment Task

Thanks for joining in for Frontend Recruitment Task @ Jobleads GmbH.  We have some work for you.
Here's a simple website build in Vue.JS made by our Junior Frotend Dev. As you can see it's not quite finished and still missing some love.
What we would like you to do is to finish the website, fix bugs and match our requirements. 

<img src="images/website.jpg" width="500">

Desired design can be found in our Sketch Output here  `./design/index.html` - you will have here all the things you need <br />
Project source files can be found in `./src/`

## Project setup and commands
```
npm install - install all dependencies [ node version 12.7.0 ]
npm run serve - Run dev enviroment
npm run build - Compiles and minifies for production
```

### Tasks to finish:
```
- Import and set Roboto font (According to the design)
- Style all the Lists / Forms / Buttons (According to the design)
- Add missing icons (According to the design)
- Create logic that show's the RegisterForm on button click and hides it (According to the design)
- Check and fix RWD (Responsive Web Design)
```

### Our Requirements:
```
- Fields in form have simple validation
- Form submit should show as simple popup/alertbox (if everything is ok then positive message, otherwise show error), and close the RegistrationForm
```

### Our hints
```
- We <3 SCSS
- For forms and icons you can use MaterialUI
- Pay attention to code quality, formatting, conventions etc. (Your code is your business card)
- If you want to change existing code - feel free - it might be also part of the task :-)
- Newset ECMAScript
- 

```

